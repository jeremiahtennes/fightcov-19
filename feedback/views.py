from django.shortcuts import render, redirect
from .forms import CreateFeedbackForm
from .models import Feedback
from . import forms

# Create your views here.
def home(request):
    return render(request, 'main/home.html')

def feedback(request):
	feedback = Feedback.objects.all()
	response = {
		'title' : 'Feedback',
        'feedback': feedback,
	}
	return render(request, 'feedback_page.html', response)

def add_feedback(request):
    response = {
        'title' : 'Add Feedback',
        'feedback_form': CreateFeedbackForm()
    }

    if request.method == "POST":
        feedback_form = CreateFeedbackForm(request.POST)
        if feedback_form.is_valid():
            feedback_form.save()
            return redirect('feedback:feedback')
    return render(request, 'add_feedback_page.html', response)

def delete_feedback(request, id):
    feedback = Feedback.objects.get(id=id)
    feedback.delete()
    return redirect('feedback:feedback')

