from django.test import TestCase, Client
from .models import Feedback
from .forms import CreateFeedbackForm

# Create your tests here.

class FeedbackTest(TestCase):
	def test_url_feedback_is_exist(self):
		response = Client().get('/feedback/')
		self.assertEqual(response.status_code,200)

	def test_feedback_using_feedback_template(self):
		response = Client().get('/feedback/')
		self.assertTemplateUsed(response, 'feedback_page.html')

	def test_if_feedback_page_return_feedback_objects(self):
		feedback = Feedback.objects.create(
			name = 'Liu Yuchen',
			email = 'liuyuchen@gmail.com',
			message = 'Hello! Keep support me'
		)

		response = Client().get('/feedback/')
		content = response.content.decode('utf8')
		self.assertIn(feedback.name, content)
		self.assertIn(feedback.email, content)
		self.assertIn(feedback.message, content)

	def test_if_add_feedback_page_return_form(self):
		response = Client().get('/feedback/add-feedback')
		content = response.content.decode('utf8')
		self.assertIn('Submit', content)

	def test_add_feedback_post(self):
		response = Client().post('/feedback/add-feedback', data={"name" : "Jonatan Christie", "email" : "jonatan_christie@gmail.com", "message" : "comeback stronger"})
		self.assertEqual(Feedback.objects.all().count(),1)


class FeedbackModelTest(TestCase):

	def test_string_representation(self):
		feedback = Feedback(name='Lee Ji Eun', email='leejieun@gmail.com', message='fighting!')
		self.assertEqual(str(feedback), feedback.name + " - " + feedback.email + " - " + feedback.message)

	def test_verbose_name_plural(self):
		self.assertEqual(str(Feedback._meta.verbose_name_plural), "feedbacks")
