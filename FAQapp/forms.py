from django import forms
from .models import modelQuestion

class formsFAQ(forms.ModelForm):
    class Meta:
        model = modelQuestion
        fields = [
            'name',
            'email',
            'question',
        ]