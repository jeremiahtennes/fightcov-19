from django.http import HttpResponseRedirect
from django.shortcuts import render,redirect
from .forms import kasusForm
from .models import Covid
# Create your views here.
def post_covcase(request):
    if request.method == 'POST':
        response_data = {}

        form = kasusForm(request.POST or None, request.FILES or None)
        if form.is_valid():
            response_data['Daerah'] = request.POST['Daerah']
            response_data['Aktif'] = request.POST['Aktif']
            response_data['Sembuh'] = request.POST['Sembuh']
            response_data['Death'] = request.POST['Death']

            data_covid = Covid(Daerah=response_data['Daerah'],
                                    Aktif=response_data['Aktif'],
                                    Sembuh=response_data['Sembuh'],
                                    Death=response_data['Death'],
    
            )
            data_covid.save()
            return redirect('/covcase/#isiCovid')
        else:
            return redirect('/covcase')
    else:
        return redirect('/covcase')

   

def covcase(request):
    # if this is a POST request we need to process the form data
    if request.method == 'POST':
        response_data = {}

        form = kasusForm(request.POST or None, request.FILES or None)
        if form.is_valid():
            response_data['Daerah'] = request.POST['Daerah']
            response_data['Aktif'] = request.POST['Aktif']
            response_data['Sembuh'] = request.POST['Sembuh']
            response_data['Death'] = request.POST['Death']

            data_covid = Covid(Daerah=response_data['Daerah'],
                                    Aktif=response_data['Aktif'],
                                    Sembuh=response_data['Sembuh'],
                                    Death=response_data['Death'],
    
            )
            data_covid.save()
            return redirect('/covcase/listCase')
        else:
            return redirect('/covcase')

    form_tambah = kasusForm()
    data = Covid.objects.all()
    response = {
        'form_tambah': form_tambah,
        'data': data,
    }
    return render(request, 'addCase.html', response)

def listCase(request):
    data = Covid.objects.all()
    response = {
        'data': data,
    }
    return render(request, 'case.html', response)

def delete_case(request,nama_daerah):
    data = Covid.objects.filter(Daerah=nama_daerah)
    data.delete()
    return redirect('/covcase')
