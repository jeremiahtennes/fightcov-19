from django import forms

class kasusForm(forms.Form):
    error_messages = {
        'required': 'This field is required.'
    }
    attrs = {
        'class': 'form-control'
    }
    Daerah = forms.CharField(widget=forms.TextInput(attrs=attrs), label='Daerah' , max_length=50, required=True)
    Aktif = forms.IntegerField(widget=forms.NumberInput(attrs=attrs), label='Aktif Kasus', required=True)
    Sembuh = forms.IntegerField(widget=forms.NumberInput(attrs=attrs), label='Angka Kesembuhan', required=True)
    Death = forms.IntegerField(widget=forms.NumberInput(attrs=attrs), label='Angka Kematian ', required=True)
   
