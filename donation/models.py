from django.db import models

# Create your models here.
class Donation(models.Model):
    nama = models.CharField("Nama ", max_length=120, null=True, blank=False)
    email =  models.CharField("Email ", max_length=120, null=True, blank=False)
    nomorHp = models.CharField("Nomor HP ", max_length=120, null=True, blank=False)
    kodePos = models.CharField("Kode pos", max_length=120, null=True, blank=False)
    nominal = models.PositiveIntegerField(default=10000)
    
    def __str__(self):
        return self.nama

