from django.test import TestCase
from django.test import Client

# Create your tests here.
class testArtikel(TestCase):
    def test_url_artikel_exist(self):
        response = Client().get("/artikel/")
        self.assertEquals(response.status_code, 200)
    
    def test_artikel_template(self):
        response = Client().get("/artikel/")
        self.assertTemplateUsed(response,'artikel/index.html')

    def test_url_formArtikel_exist(self):
        response = Client().get("/artikel/FormArtikel/")
        self.assertEquals(response.status_code, 200)

    def test_formArtikel_template(self):
        response = Client().get("/artikel/FormArtikel/")
        self.assertTemplateUsed(response,'artikel/formArtikel.html')

