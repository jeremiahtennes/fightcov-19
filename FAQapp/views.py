from django.http import HttpResponseRedirect
from django.shortcuts import render
from .forms import formsFAQ
from .models import modelQuestion

# Create your views here.

def forms_FAQ(request):
    if request.method == 'POST':
        form = formsFAQ(request.POST or None)
        if form.is_valid():
            form.save()
            objectFAQ = modelQuestion.objects.all()
            return HttpResponseRedirect(redirect_to='/FAQ/')
        else:
            objectFAQ = modelQuestion.objects.all()
            return render(request, 'forms_FAQ.html', {'form': form,'database': objectFAQ})
    else:
        form = formsFAQ()
        objectFAQ = modelQuestion.objects.all()
        return render(request, 'forms_FAQ.html', {'form': form,'database': objectFAQ})

def FAQ(request):
    objectFAQ = modelQuestion.objects.all()
    return render(request, 'FAQ.html', {'database': objectFAQ})