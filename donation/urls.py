from django.urls import path
from .views import donation, donation_form

app_name = 'donation'

urlpatterns = [
    path('', donation, name='donation'),
    path('donate/', donation_form, name='donation_form'),
]