from django.test import TestCase

# Create your tests here.
from django.test import Client
from django.urls import resolve

from .models import Covid
from . import views
from .forms import kasusForm

class UnitTestCovCase(TestCase):
    def test_response_page(self):
        response = Client().get('/covcase/')
        self.assertEqual(response.status_code,200)
    
    def test_url_listCase(self):
        response = Client().get('/covcase/listCase')
        self.assertEquals(response.status_code, 200)

    def test_template1_used(self):
        response = Client().get('/covcase/')
        self.assertTemplateUsed(response, 'addCase.html')
    
    def test_template2_used(self):
        response = Client().get('/covcase/listCase/')
        self.assertTemplateUsed(response, 'baseCase.html')
    
    def test_template2_used(self):
        response = Client().get('/covcase/')
        self.assertTemplateUsed(response, 'baseCase.html')
    
    def test_func_page(self):
        found = resolve('/covcase/')
        self.assertEqual(found.func, views.covcase)
    
    def test_covcase_model_Covid(self):
        Covid.objects.create(Daerah = "Banten", Aktif=100, Sembuh=50, Death=40)
        total = Covid.objects.all().count()
        self.assertEquals(total, 1)
    
    def test_valid_postCov_method(self):
        form = kasusForm(data = {'Daerah':'Banten', 'Aktif':100,'Sembuh':50,'Death':40})
        self.assertTrue(form.is_valid())
        response = self.client.post('/covcase/listCase')
        self.assertEqual(response.status_code,200)
        
    def test_modelCov_to_html(self):
        cases = Covid.objects.create(Daerah = "Banten", Aktif=100, Sembuh=50, Death=40)
        response = Client().get('/covcase/listCase')
        isi_html = response.content.decode('utf8')
        self.assertIn(str(cases), isi_html)