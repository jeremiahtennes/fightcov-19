from django.db import models

# Create your models here.

class Feedback(models.Model):
	name = models.CharField(max_length=200)
	email = models.CharField(max_length=200)
	message = models.TextField()

	def __str__(self):
		return "{} - {} - {}".format(self.name, self.email, self.message)
