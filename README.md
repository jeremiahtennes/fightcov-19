# fightcov-19
Anggota kelompok G03:<br>
1806204966	Jeremiah Garcia Tennes<br>
1906302586	Alvito Athallah<br>
1906302560	Marta Junike Dewi Anugerah<br>
1906299206	Yolanda Emanuella Sutanto<br>
1906299004	Meutia Khalbi Yuventa<br>
<br>
Status Pipelines :<br>
[![pipeline status](https://gitlab.com/jeremiahtennes/fightcov-19/badges/master/pipeline.svg)](https://gitlab.com/jeremiahtennes/fightcov-19/commits/master)
<br>
<br>
Status Code Coverage :<br>
[![coverage report](https://gitlab.com/jeremiahtennes/fightcov-19/badges/master/coverage.svg)](https://gitlab.com/jeremiahtennes/fightcov-19/commits/master)
<br>
<br>
Link website :<br>
https://fightcov-19.herokuapp.com
<br>
<br>
## Essentials:<br>
[Wireframe](https://www.figma.com/file/8tmKj7iEmc20uNmArOftgC/WIREFRAME-TK-1-PPW?node-id=0%3A1)<br>
[High Fidelity Mockup](https://www.figma.com/file/8tmKj7iEmc20uNmArOftgC/WIREFRAME-TK-1-PPW?node-id=16%3A226)<br>
[Persona](https://www.figma.com/file/8tmKj7iEmc20uNmArOftgC/WIREFRAME-TK-1-PPW?node-id=23%3A0)<br>


## Fitur - fitur Web COVID-19:<br>
1. Buat akun<br>
2. Page masukin data kasus covid<br>
3. Artikel<br>
4. Donasi (form buat donate)<br>
5. Cara penyebaran covid-19 virus<br>
6. Ciri-ciri awal penyakit<br>
7. Cara pencegahan<br>
8. Dashboard data kasus covid<br>
9. Contact (emergency)<br>

## Latar belakang:<br>
Kami membuat web yang bernama fightcov-19 yang sangat berkaitan dengan kondisi yang kita semua sedang alami saat ini. Web kami memberikan banyak manfaat dan informasi terkait Covid-19 bagi semua orang yang mengaksesnya. 

Tujuan web ini dibuat adalah agar semua orang dapat lebih aware lagi terhadap penyebaran Covid-19, sekaligus juga memberikan informasi terkait berita-berita terbaru dari penyebaran Covid-19. Selain itu, orang-orang juga dapat ikut membantu korban dari Covid-19 dengan memberikan donasi yang akan digunakan untuk biaya rumah sakit, obat-obatan, dan lain-lain untuk membantu memutus rantai penyebaran Covid-19 di Indonesia. Di dalam web ini juga kami akan menampilkan contact (emergency) apabila terjadi sesuatu yang tidak diinginkan, sehingga pertolongan pertama bisa segera langsung dilakukan. 

Semoga dengan adanya web yang kami buat bisa menimbulkan kesadaran kepada orang-orang tentang betapa bahayanya Covid-19, dan orang-orang sebisa mungkin dapat melakukan pencegahan yang ada, sehingga pada akhirnya Indonesia dan seluruh negara lainnya bisa cepat terbebas dari virus Covid-19 ini.


