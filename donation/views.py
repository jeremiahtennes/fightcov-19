from django.shortcuts import render, redirect
from .models import Donation
from .forms import DonationForm
from django.db.models import Sum

# Create your views here.
def donation_form(request):
    form = DonationForm()
    if request.method =="POST":
        form=DonationForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect('donation:donation')
    return render(request, "form_donate.html", {'form':form})

def donation(request):
    count = Donation.objects.all().count()
    donationsum = Donation.objects.aggregate(Sum('nominal')).get('nominal__sum')
    response = {
        'count' : count,
        'donationsum' : donationsum,
    }
    return render(request, "donation.html", response)