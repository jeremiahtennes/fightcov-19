from django.shortcuts import render, redirect
from django import forms
from .models import Feedback 
#from . import forms
from django.forms import ModelForm
# from . import models

class CreateFeedbackForm(forms.ModelForm):
	class Meta:
		model = Feedback 
		fields = [
			"name",
			"email",
			"message",
		]

	def __init__(self, *args, **kwargs):
		super().__init__(*args, **kwargs)
		for field in self.Meta.fields:
			self.fields[field].widget.attrs.update({
				'class': 'form-control',
			})
