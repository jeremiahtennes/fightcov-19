from django.urls import path
from .views import FAQ, forms_FAQ

app_name = 'FAQapp'

urlpatterns = [
    path('', FAQ, name='FAQ'),
    path('form-FAQ/', forms_FAQ, name='forms_FAQ'),
]