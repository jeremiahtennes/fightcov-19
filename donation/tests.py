from django.test import TestCase, Client
from django.urls import resolve
from django.apps import apps
from .views import donation_form, donation
from .models import Donation
from .apps import DonationConfig

# Create your tests here.

class TestApp(TestCase):
    def test_apps(self):
        self.assertEqual(DonationConfig.name, 'donation')
        self.assertEqual(apps.get_app_config('donation').name, 'donation')
    
    def test_url_donation_exist(self):
        response = Client().get('/donation/')
        self.assertEquals(response.status_code, 200)
    
    def test_donation_index_func(self):
        found = resolve('/donation/')
        self.assertEqual(found.func, donation)
    
    def test_donation_using_template(self):
        response = Client().get('/donation/')
        self.assertTemplateUsed(response, 'donation.html')
    
    def test_donation_model_create_new_object(self):
        donation = Donation(nama='John Doe', email='johndoe@jd.com', nomorHp='08123456789', kodePos='12345', nominal=10000)
        donation.save()
        self.assertEqual(Donation.objects.all().count(), 1)
    
    def test_donate_url_exist(self):
        response = Client().get('/donation/donate/')
        self.assertEqual(response.status_code, 200)
        
    def test_donate_index_func(self):
        found = resolve('/donation/donate/')
        self.assertEqual(found.func, donation_form)
        
    def test_donate_using_template(self):
        response = Client().get('/donation/donate/')
        self.assertTemplateUsed(response, 'form_donate.html')
        
    def test_str_is_equal_to_title(self):
        donation = Donation(nama='John Doe', email='johndoe@jd.com', nomorHp='08123456789', kodePos='12345', nominal=10000)
        donation.save()
        object = Donation.objects.get(pk=1)
        self.assertEqual(str(object), object.nama)
        
    