from django.db import models

# Create your models here.
class modelQuestion(models.Model):
    name = models.CharField("Name ",max_length=120, null=True)
    email = models.EmailField("Email ",max_length=120, null=True)
    question = models.TextField("Question ",max_length=500, null=True, blank=False)
    answer = models.TextField("Answer ",default="Thank you for the question!",max_length=1000, null=True, blank=False)

    def __str__(self):
        return f'{self.name}'
